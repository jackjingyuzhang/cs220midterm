/*
 * wave.c
 * Written by bstride1 and jzhan237
 * Finished 15 Oct 2019
 * write_wave_header and read_wave_header were given--they were not written by us.
 * This file contains functions to write sound waves into a buffer.
 * The sound waves include sine, square, and saw.
 * These can be used either with stereo sound or without, and are mostly used without
 * stereo sound by other programs such as render_tone.c and render_song.c
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "io.h"
#include "wave.h"

/*
 * bstride1
 * Checks if the destination += val would exceed the bounds of int16_t.
 * If it would, returns max or min of int16_t. Otherwise, returns sum.
 */
int16_t get_adjusted(int16_t destination, int16_t val){
  // make a variable that can hold about max and min
  int32_t temp = (int32_t)destination + (int32_t)val;
  if (temp > INT16_MAX){
    return INT16_MAX;
  }
  else if (temp < INT16_MIN){
    return INT16_MIN;
  }
  else{
    return temp;
  }
}

/*
 * Write a WAVE file header to given output stream.
 * Format is hard-coded as 44.1 KHz sample rate, 16 bit
 * signed samples, two channels.
 *
 * Parameters:
 *   out - the output stream
 *   num_samples - the number of (stereo) samples that will follow
 */
void write_wave_header(FILE *out, unsigned num_samples) {
  /*
   * See: http://soundfile.sapp.org/doc/WaveFormat/
   */

  uint32_t ChunkSize, Subchunk1Size, Subchunk2Size;
  uint16_t NumChannels = NUM_CHANNELS;
  uint32_t ByteRate = SAMPLES_PER_SECOND * NumChannels * (BITS_PER_SAMPLE/8u);
  uint16_t BlockAlign = NumChannels * (BITS_PER_SAMPLE/8u);

  /* Subchunk2Size is the total amount of sample data */
  Subchunk2Size = num_samples * NumChannels * (BITS_PER_SAMPLE/8u);
  Subchunk1Size = 16u;
  ChunkSize = 4u + (8u + Subchunk1Size) + (8u + Subchunk2Size);

  /* Write the RIFF chunk descriptor */
  write_bytes(out, "RIFF", 4u);
  write_u32(out, ChunkSize);
  write_bytes(out, "WAVE", 4u);

  /* Write the "fmt " sub-chunk */
  write_bytes(out, "fmt ", 4u);       /* Subchunk1ID */
  write_u32(out, Subchunk1Size);
  write_u16(out, 1u);                 /* PCM format */
  write_u16(out, NumChannels);
  write_u32(out, SAMPLES_PER_SECOND); /* SampleRate */
  write_u32(out, ByteRate);
  write_u16(out, BlockAlign);
  write_u16(out, BITS_PER_SAMPLE);

  /* Write the beginning of the "data" sub-chunk, but not the actual data */
  write_bytes(out, "data", 4);        /* Subchunk2ID */
  write_u32(out, Subchunk2Size);
}

/*
 * Read a WAVE header from given input stream.
 * Calls fatal_error if data can't be read, if the data
 * doesn't follow the WAVE format, or if the audio
 * parameters of the input WAVE aren't 44.1 KHz, 16 bit
 * signed samples, and two channels.
 *
 * Parameters:
 *   in - the input stream
 *   num_samples - pointer to an unsigned variable where the
 *      number of (stereo) samples following the header
 *      should be stored
 */
void read_wave_header(FILE *in, unsigned *num_samples) {
  char label_buf[4];
  uint32_t ChunkSize, Subchunk1Size, SampleRate, ByteRate, Subchunk2Size;
  uint16_t AudioFormat, NumChannels, BlockAlign, BitsPerSample;

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "RIFF", 4u) != 0) {
    fatal_error("Bad wave header (no RIFF label)");
  }

  read_u32(in, &ChunkSize); /* ignore */

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "WAVE", 4u) != 0) {
    fatal_error("Bad wave header (no WAVE label)");
  }

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "fmt ", 4u) != 0) {
    fatal_error("Bad wave header (no 'fmt ' subchunk ID)");
  }

  read_u32(in, &Subchunk1Size);
  if (Subchunk1Size != 16u) {
    fatal_error("Bad wave header (Subchunk1Size was not 16)");
  }

  read_u16(in, &AudioFormat);
  if (AudioFormat != 1u) {
    fatal_error("Bad wave header (AudioFormat is not PCM)");
  }

  read_u16(in, &NumChannels);
  if (NumChannels != NUM_CHANNELS) {
    fatal_error("Bad wave header (NumChannels is not 2)");
  }

  read_u32(in, &SampleRate);
  if (SampleRate != SAMPLES_PER_SECOND) {
    fatal_error("Bad wave header (Unexpected sample rate)");
  }

  read_u32(in, &ByteRate); /* ignore */

  read_u16(in, &BlockAlign); /* ignore */

  read_u16(in, &BitsPerSample);
  if (BitsPerSample != BITS_PER_SAMPLE) {
    fatal_error("Bad wave header (Unexpected bits per sample)");
  }

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "data", 4u) != 0) {
    fatal_error("Bad wave header (no 'data' subchunk ID)");
  }

  /* finally we're at the Subchunk2Size field, from which we can
   * determine the number of samples */
  read_u32(in, &Subchunk2Size);
  *num_samples = Subchunk2Size / NUM_CHANNELS / (BITS_PER_SAMPLE/8u);
}
/*
 * bstride1 jzhan237
 * Render sine audio waveform (44.1KHz, 16bit)
 *
 * Parameters:
 *   buf - pointer to an array of int_16t, which represent the audio stream
 *   num_samples - number of samples need to be generated; determine the durating of the audio waveform
 *   channel - indicate which channel to generate (0 = left, 1 = right)
 *   freq_hz - frequency of generated waveform in hertz
 *   amplitude - relative amplitude of the generated waveform; max is 1.0f
 * 
 * Equation:
 *   p = a × sin(tf × 2π)
 * 
 * Use float sinf(float arg); to compute sine
 * 
 * float p = amplitude * INT16_MAX * sinf(t * freq_hz * 2 * PI);
 * where t is [I HAVE NOT CALCULATED THIS YET]
 */
void render_sine_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude){
  // Add values into buf
  // If channel is left, add numsamples times into even slots
  // If right, add numsamples times into odd slots
  for (unsigned i = channel; i < num_samples * 2; i+=2){
    // current time at sample
    // i / 2 is current sample we're on
    float time = (i / 2) * 1.0 / SAMPLES_PER_SECOND;

    float val = (amplitude * sinf(time * freq_hz * 2 * PI));

    buf[i] = get_adjusted(buf[i], (int16_t)(val * INT16_MAX));
  }
}
/*
 * bstride1
 * Renders the sine wave into both channels of the buffer.
 */
void render_sine_wave_stereo(int16_t buf[], unsigned num_samples,float freq_hz, float amplitude){
  render_sine_wave(buf, num_samples, 0u, freq_hz, amplitude); // left channel
  render_sine_wave(buf, num_samples, 1u, freq_hz, amplitude); // right channel
}


/*
 * bstride1
 * Renders  a square wave into the indicated channel of the buffer.
 * Channel:
 * LEFT = 0, RIGHT = 1
 * If a sine wave would add a positive value, this adds the max positive value.
 * If a sine wave would add a negative value, this adds the max negative value.
 * Each "max" is scaled by amplitude.
 * total time = (num_samples * 1.0) / SAMPLES_PER_SECOND
 */
void render_square_wave(int16_t buf[], unsigned num_samples, unsigned channel,float freq_hz, float amplitude){
  // Add values into buf
  // If channel is left, add numsamples times into even slots
  // If right, add numsamples times into odd slots
  for (unsigned i = channel; i < num_samples * 2; i+=2){
    // current time at sample
    // i / 2 is current sample we're on
    float time = (i / 2) * 1.0 / SAMPLES_PER_SECOND;
    
    if (sinf(time * freq_hz * 2 * PI) >= 0){ // sin would add pos value
      // add max value into buf
      buf[i] = get_adjusted(buf[i], (int16_t)(amplitude * INT16_MAX));
    } else { // sin would add negative value
      buf[i] = get_adjusted(buf[i], (int16_t)(amplitude * INT16_MIN));
    } 
  }
}

/*
 * bstride1
 * Renders a square wave into both channels of the buffer
 */
void render_square_wave_stereo(int16_t buf[], unsigned num_samples,float freq_hz, float amplitude){
  render_square_wave(buf, num_samples, 0u, freq_hz, amplitude); // left channel
  render_square_wave(buf, num_samples, 1u, freq_hz, amplitude); // right channel
}

/* 
 * jzhan237
 * Renders a saw wave into the specified channel of the buffer.
 * A saw wave starts at the max negative value and increases linearly to the
 * max positive value, then drops back to the max negative value.
 * The "max" values are scaled by amplitude.
 * Unfinished - practially just copied in render_square_wave
 */
void render_saw_wave(int16_t buf[], unsigned num_samples, unsigned channel,float freq_hz, float amplitude){
  
  // how long is each cycle in seconds
  float cycle_length = 1.0 / freq_hz;

  // slope of linear increase is change in y over change in t
  // (max - min) / 1 because we only consider 1 cycle length
  float slope = (INT16_MAX - INT16_MIN);

  // Add values into buf
  // If channel is left, add numsamples times into even slots
  // If right, add numsamples times into odd slots
  for (unsigned i = channel; i < num_samples * 2; i+=2){
    // time at current sample is current sample divided by SAMPLES_PER_SECOND
    float time = (i / 2) * 1.0 / SAMPLES_PER_SECOND;
    // relative time in a period; 0 <= time_in_period <= 1

    // this get only the decimal points of time / cycle_length (which is the period we are on)
    float time_in_period = time / cycle_length - (int)(time / cycle_length);
    // eq val = slope * time_in_period + INT16_MIN
    // for a single period ^ 
    float val = slope * time_in_period + INT16_MIN;

    buf[i] = get_adjusted(buf[i], (int16_t)(amplitude * val));
  }
}

/*
 * bstride1
 * Renders a saw wave into both channels of the buffer
 */
void render_saw_wave_stereo(int16_t buf[], unsigned num_samples,float freq_hz, float amplitude){
  render_saw_wave(buf, num_samples, 0u, freq_hz, amplitude); // left channel
  render_saw_wave(buf, num_samples, 1u, freq_hz, amplitude); // right channel
}

/*
 * bstride1
 * Renders the type of wave as specified by voice into the indicated channel
 * of the buffer.
 * Channel:
 * LEFT = 0, RIGHT = 1
 * Voice:
 * SINE = 0, SQUARE = 1, SAW = 2
 */
void render_voice(int16_t buf[], unsigned num_samples, unsigned channel,float freq_hz, float amplitude, unsigned voice){
  switch(voice) {
  case 0u:
    render_sine_wave(buf, num_samples, channel, freq_hz, amplitude);
    break;
  case 1u:
    render_square_wave(buf, num_samples, channel, freq_hz, amplitude);
    break;
  case 2u:
    render_saw_wave(buf, num_samples, channel, freq_hz, amplitude);
    break;
  default:
    fatal_error("Invalid voice in render_voice.\n");
  }
}

/*
 * bstride1
 * Renders the type of wave as specified by voice into both channels the buffer
 */
void render_voice_stereo(int16_t buf[], unsigned num_samples, float freq_hz,float amplitude, unsigned voice){
  switch(voice) {
  case 0u:
    render_sine_wave_stereo(buf, num_samples, freq_hz, amplitude);
    break;
  case 1u:
    render_square_wave_stereo(buf, num_samples, freq_hz, amplitude);
    break;
  case 2u:
    render_saw_wave_stereo(buf, num_samples, freq_hz, amplitude);
    break;
  default:
    fatal_error("Invalid voice in render_voice_stereo.\n");
  }
}
