#ifndef RENDER_SONG_H
#define RENDER_SONG_H

#include <stdint.h>

float MIDI_freq(int n);
void check_overload(unsigned cu_samples, unsigned num_samples);

#endif
