/*
 * render_song.c
 * Written by bstride1 and jzhan237.
 * Started 13 Oct 2019.
 * Finished 15 Oct 2019 9:41pm.
 * This program takes an input file and an output file.
 * Input file:
 *    A text file that starts with the number of samples in the song, followed
 *    by the number of samples per beat.
 *    Following that are a list of commands that tell what notes to play.
 * Output file:
 *    A .wav file for the song to be written into
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "io.h"
#include "wave.h"
#include "render_song.h"

/*
 * bstride1
 * MIDI note number n is converted to frequency by:
 * f = 440 * 2^((n-69)/12)
 */
float MIDI_freq(int n){
  return 440 * powf(2, (n - 69) * 1.0 / 12);
}

/*
 * bstride1
 * Checks if the program is trying to add more samples/beats than expected.
 * Exits program with an error message if true.
 */
void check_overload(unsigned cu_samples, unsigned num_samples){
  if (cu_samples > num_samples){ // if about to go out of bounds
    fatal_error("Too many samples input in render_song.\n");
  }
}

/*
 * bstride1 jzhan237
 * This should be executed with the following command line arguments in the following order:
 * 0. Executable
 * 1. songinput - text file
 * 2. waveoutput - wav file
 */
int main(int argc, char* argv[]){
  if(argc != 3){
    fatal_error("Incorrect number of inputs for render_song.\n");
  }

  FILE* input = fopen(argv[1], "r");
  FILE* wavfileout = fopen(argv[2], "wb");
  
  if (input == NULL){
    fatal_error("Could not open input song file.\n");
  }
  if (wavfileout == NULL){
    fatal_error("Could not open output song file.\n");
  }

  // read the first two values in songinput: number of samples and samples per beat
  unsigned num_samples;
  unsigned samples_per_beat;;
  fscanf(input,"%u %u", &num_samples, &samples_per_beat);

  // create the buffer and initialize to 0
  int16_t* buf = (int16_t*) calloc(num_samples * 2, sizeof(int16_t));
  if(buf == NULL){
    fatal_error("Failed to allocate memory");
  }

  // Create a pointer to track location in buf
  int16_t* pbuf = buf;

  // default to sine wave unless otherwise noted
  unsigned voice = SINE;

  // default to 0.1 amplitude unless otherwise noted
  float amplitude = 0.1;

  // variables that are used multiple times in the following switch statement
  float b; // number of beats to play for
  unsigned new_samples; // number of samples to play for
  unsigned n; // MIDI note number
  
  // this is the cumulative number of samples that are trying to be put into buf
  unsigned cu_samples = 0;

  // now the input file will have lines starting with a char of any of the following:
  // N C P V A
  // dir for directive
  char dir;
  while(fscanf(input, " %c", &dir) != EOF){
    switch(dir){
    case 'N': // play MIDI note number n for b beats
      fscanf(input, " %f %u", &b, &n);
      new_samples = b * samples_per_beat;
      cu_samples += new_samples;
      check_overload(cu_samples, num_samples);
      // render a voice into the buffer with given characteristics
      render_voice_stereo(pbuf, new_samples, MIDI_freq(n), amplitude, voice);
      pbuf += new_samples * 2; // move pointer over the number of samples we just added
      break;
    case 'C': ; // play notes for b beats
      fscanf(input, " %f", &b);
      new_samples = b * samples_per_beat;
      cu_samples += new_samples;
      check_overload(cu_samples, num_samples);
      
      int done = 0;
      while(!done){ // continue until note 999 found
	fscanf(input, " %u", &n); // read next note into n
	if(n == 999){
	  done = !done;
	} else { // good note read => add into buf
	  render_voice_stereo(pbuf, new_samples, MIDI_freq(n), amplitude, voice);
	}
      }
      pbuf += new_samples * 2; // increment pointer
      break;
    case 'P': // pause for b beats
      fscanf(input, " %f", &b);
      new_samples = b * samples_per_beat;
      cu_samples += new_samples;
      check_overload(cu_samples, num_samples);
      pbuf += new_samples * 2; // move pointer over for number of samples we just paused for
      break;
    case 'V': // change to specified voice
      fscanf(input, " %u", &voice);
      break;
    case 'A': // change to specified amplitude
      fscanf(input, " %f", &amplitude);
      break;
    default:
      fatal_error("Unrecognized symbol in song file.\n");
    }
  }
  
  // write the contents into wavfileout
  write_wave_header(wavfileout, num_samples);
  write_s16_buf(wavfileout, buf, num_samples * 2);

  free(buf);
  fclose(input);
  fclose(wavfileout);
  return 0;
}
