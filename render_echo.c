// jzhan237 (small edits by bstride1)
// 14 Oct 2019
/* render_echo.c
 * The program processes an input WAVE file to generate an echo effect
 * 
 * Invocation: 
 *   ./render_echo wavfilein wavfileout delay amplitude
 * 
 * Parameters:
 *   wavfilein - name of the input wave file
 *
 *   wavfileout - name of the output wave file
 *
 *   delay - an integer delay, specified as a number of samples
 *
 *   amplitude - a floating point value range front 0.0 to 1.0,
 *    specifying relative amplitude of the echo effect
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "io.h"
#include "wave.h"

int main(int argc, char *argv[]){
	// process command line arguments
	if(argc != 5){
		fatal_error("Usage: ./render_echo wavfilein wavfileout delay amplitude");
	}
	FILE* wavfilein = fopen(argv[1], "rb");
	if(wavfilein == NULL){
		fatal_error("Could not open wave file");
	}
	FILE* wavfileout = fopen(argv[2], "wb");
	if(wavfileout == NULL){
		fatal_error("Could not write wave file");
	}
	unsigned delay = atol(argv[3]);
	float amplitude = atof(argv[4]);
	
	// read wave header
	unsigned num_samples;
	read_wave_header(wavfilein, &num_samples);

	// read wave data
	int16_t* buf = (int16_t*) malloc(sizeof(int16_t) * num_samples * 2);
	if(buf == NULL){
		fatal_error("Failed to allocate memory");
	}
	read_s16_buf(wavfilein, buf, num_samples * 2);

	// process new wave data
	// the new wave data need to be produced in reverse order
	// to avoid using another array to copy the new value
	// 
	// if data is not produced in reverse order, we could not
	// access the orignal data because buf stores post-processed
	// data, which overwrites the original values in buf
	for(unsigned i = num_samples * 2 - 1; i >= delay * 2; i--){
		int16_t val = (int16_t)(amplitude * buf[i - delay * 2]);
		buf[i] = get_adjusted(buf[i], val);
	}

	// write new wave file
	write_wave_header(wavfileout, num_samples);
	write_s16_buf(wavfileout, buf, num_samples * 2);

	// remember to deallocate and fclose!
	free(buf);
	fclose(wavfilein);
	fclose(wavfileout);
	return 0;
}



