/*
 * io.c
 * Written by bstride1 and jzhan237
 * Finished 15 Oct 2019
 * This includes a variety of functions that read and write different types
 * in binary. 
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "io.h"

/*
 * bstride1
 * Prints the error message to stderr
 */ 
void fatal_error(const char *message){
  fprintf(stderr, "Error: %s", message);
  exit(1);
}

/*
 * bstride1 jzhan237
 * Reads a single char and stores the value where val points
 */
void read_byte(FILE *in, char *val){
  int c = fgetc(in);

  // check error occurs when fgetc
  if(c == EOF){
    fatal_error("Error - could not read byte");
  }

  *val = (char)c;
}

/*
 * bstride1
 * Reads n bytes from the file and stores them in data[].
 * If n bytes cannot be read, calls fatal_error
 */
void read_bytes(FILE *in, char data[], unsigned n){
  // call read_byte n times
  for (unsigned i = 0; i < n; i++){
    read_byte(in, &(data[i]));
  }
}

/*
 * bstride1 jzhan237
 * Reads a single uint16_t from the file and stores where val points
 */
void read_u16(FILE *in, uint16_t *val){
  char b[2];
  read_bytes(in, b, 2);
  // now that we have the two bytes, combine them and store these binary operations as using big endian
  uint16_t temp0 = b[0] & 0xFF, temp1 = b[1] & 0xFF;
  *val = temp0 + temp1 * 256u;
}

/* 
 * bstride1 jzhan237
 * Reads a single uint32_t from the file and stores it where val points
 */
void read_u32(FILE *in, uint32_t *val){
  // create an array of the bytes
  char b[4];
  read_bytes(in, b, 4);

  // combine the bytes
  uint32_t total = 0u;
  for (unsigned i = 0; i < 4; i++){
    // adds to the total the byte shifted over by 8*i, which is the number of bits before the current byte
    uint32_t temp = b[i] & 0xFF; // 0xFF is a mask to make sure all the bits after the first byte is 0...0
    for(unsigned j = 0; j < i; j++){
      temp *= 256u;
    }
    total += temp;
  }
  // store the total where val points
  *val = total;
}

/*
 * bstride1 jzhan237
 * Reads a single sint16_t (signed 16 bit integer) and stores it
 * where val points
 */
void read_s16(FILE *in, int16_t *val){
  char b[2];
  read_bytes(in, b, 2);
  int16_t temp0 = b[0] & 0xFF, temp1 = b[1] & 0xFF;
  *val = temp0 + temp1 * 256u;
}

/*
 * bstride1
 * Reads a sequence of n signed 16 bit integers (in the file as little endian)
 * and stores them in the array
 */
void read_s16_buf(FILE *in, int16_t buf[], unsigned n){
  // just call read_s16 n times and tell it to store in the array
  for (unsigned i = 0; i < n; i++){
    read_s16(in, &(buf[i]));
  }
}

// jzhan237
// Write a single byte of data to file handle "out"
void write_byte(FILE *out, char val) {
  int flag = fputc(val, out);

  // check if error occurs when fputc
  if(flag == EOF){
    fatal_error("Error - could not write byte");
  }

}

// jzhan237
// Write n bytes of data to file handle "out"
void write_bytes(FILE *out, const char data[], unsigned n) {
  for(unsigned i = 0; i < n; i++) {
    write_byte(out, data[i]);
  }
}

// jzhan237
// Write a uint16_t value to file handle "out" in little endian order
void write_u16(FILE *out, uint16_t value) {
  write_byte(out, (value & 0xFF));
  write_byte(out, ((value >> 8) & 0xFF));
}

// jzhan237
// Write a uint32_t value to file handle "out" in little endian order
void write_u32(FILE *out, uint32_t value) {
  for(unsigned i = 0; i < 4; i++){
    write_byte(out, ((value >> (8 * i)) & 0xFF));
  }
}

// jzhan237
// Write a int16_t value to file handle "out" in little endian order
void write_s16(FILE *out, int16_t value) {
  write_byte(out, (value & 0xFF));
  write_byte(out, ((value >> 8) & 0xFF));
}

// jzhan237
// Write n int16_t values to file handle "out" in little endian order
void write_s16_buf(FILE *out, const int16_t buf[], unsigned n) {
  for(unsigned i = 0; i < n; i++) {
    write_s16(out, buf[i]);
  }
}
