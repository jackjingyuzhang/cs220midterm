CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -Wextra

all: render_tone render_song render_echo

render_echo: render_echo.o io.o wave.o
	$(CC) $(CFLAGS) -o render_echo render_echo.o wave.o io.o -lm

read_wave: read_wave.o wave.o io.o
	$(CC) $(CFLAGS) -o read_wave read_wave.o wave.o io.o -lm

render_tone: io.o wave.o render_tone.o
	$(CC) $(CFLAGS) -o render_tone wave.o io.o render_tone.o -lm

render_song: io.o wave.o render_song.o
	$(CC) $(CFLAGS) -o render_song wave.o io.o render_song.o -lm

render_echo.o: render_echo.c wave.h io.h render_song.h
	$(CC) $(CFLAGS) -c render_echo.c

render_song.o: render_song.c wave.h io.h
	$(CC) $(CFLAGS) -c render_song.c

render_tone.o: render_tone.c wave.h io.h
	$(CC) $(CFLAGS) -c render_tone.c

read_wave.o: read_wave.c wave.h io.h
	$(CC) $(CFLAGS) -c read_wave.c

wave.o: wave.c wave.h io.h # has <math.h>, so remember to add -lm when linking
	$(CC) $(CFLAGS) -c wave.c

test_io: test_io.o io.o
	$(CC) $(CFLAGS) -o test_io test_io.o io.o
	./test_io
	rm -f test_byte.dat test_bytes.dat test_s16.dat test_s16_buf.dat test_u16.dat test_u32.dat

test_io.o: test_io.c io.h test_io.h
	$(CC) $(CFLAGS) -c test_io.c

io.o: io.c io.h
	$(CC) $(CFLAGS) -c io.c

clean:
	rm -f *.o test_io read_wave render_tone render_song render_echo
