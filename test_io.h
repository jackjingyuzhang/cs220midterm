#ifndef TEST_IO_H
#define TEST_IO_H

void test_write_byte();
void test_write_bytes();
void test_write_u16();
void test_write_u32();
void test_write_s16();
void test_write_s16_buf();

void test_read_byte();
void test_read_bytes();
void test_read_u16();
void test_read_u32();
void test_read_s16();
void test_read_s16_buf();

#endif
