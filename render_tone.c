/*
 * render_tone.c
 * Written by bstride1 and jzhan237
 * Started 12 Oct 2019
 * Finished 15 Oct 2019 (very few changes since start date)
 * This program takes the specifics of a wave and renders it.
 * It renders only a single type of wave--it cannot create a
 * combination of waves like render_song.c
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "io.h"
#include "wave.h"

int main(int argc, char *argv[]){
  /* This program is invoked as:
   * ./render_tone voice frequency amplitude numsamples wavfileout
   * so there are 6 arguments
   */
  if (argc != 6){
    fatal_error("Incorrect number of arguments for render_tone.\n");
  }

  // I'll use atof() and atol() from stdlib.h to convert stings in argv[] to nums

  unsigned voice = atol(argv[1]);
  float frequency = atof(argv[2]);
  float amplitude = atof(argv[3]);
  unsigned num_samples = atol(argv[4]);

  // need num_sample number of pairs
  // make sure that buf contains zeros. We don't want it starting with random values
  int16_t* buf = (int16_t*) calloc(num_samples * 2, sizeof(int16_t));
  if(buf == NULL){
    fatal_error("Failed to allocate memory");
  }

  FILE* wavfileout = fopen(argv[5], "wb");

  if (wavfileout == NULL){
    fatal_error("Could not open file for output of render_tone.\n");
  }

  // render the values into the buffer
  render_voice_stereo(buf, num_samples, frequency, amplitude, voice);

  // write into file
  write_wave_header(wavfileout, num_samples);
  write_s16_buf(wavfileout, buf, num_samples * 2);
  
  free(buf);
  fclose(wavfileout);
  return 0;
}
