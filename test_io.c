#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include "io.h"
#include "test_io.h"

int main(void) {
	// jzhan237
	printf("Calling test_write_byte()\n\n");
	test_write_byte();
	printf("Passed test_write_byte()\n");

	printf("Calling test_write_bytes()\n\n");
	test_write_bytes();
	printf("Passed test_write_bytes()\n");

	printf("Calling test_write_u16()\n\n");
	test_write_u16();
	printf("Passed test_write_u16()\n");

	printf("Calling test_write_u32()\n\n");
	test_write_u32();
	printf("Passed test_write_u32()\n");

	printf("Calling test_write_s16()\n\n");
	test_write_s16();
	printf("Passed test_write_s16()\n");

	printf("Calling test_write_s16_buf()\n\n");
	test_write_s16_buf();
	printf("Passed test_write_s16_buf()\n");

	// bstride1
	printf("Calling test_read_byte()\n\n");
	test_read_byte();
	printf("Passed test_read_byte()\n");

	printf("Calling test_read_bytes()\n\n");
	test_read_bytes();
	printf("Passed test_read_bytes()\n");

	printf("Calling test_read_u16()\n\n");
	test_read_u16();
	printf("Passed test_read_u16()\n");

	printf("Calling test_read_u32()\n\n");
	test_read_u32();
	printf("Passed test_read_u32()\n");

	printf("Calling test_read_s16()\n\n");
	test_read_s16();
	printf("Passed test_read_s16()\n");

	printf("Calling test_read_s16_buf()\n\n");
	test_read_s16_buf();
	printf("Passed test_read_s16_buf()\n");

	return 0;
}

// the following is written by jzhan237
/* name of the test files:
 * test_byte.dat
 * test_bytes.dat
 * test_u16.dat
 * test_u32.dat
 * test_s16.dat
 * test_s16_buf.dat
 * we use write functions to create and write these files
 * then we read them with read functions
 */


void test_write_byte(){
	FILE* out = fopen("test_byte.dat", "wb");
	char val[] = {'a', 'b'};
	write_byte(out, val[0]);
	write_byte(out, val[1]);
	fclose(out);
}

void test_write_bytes(){
	FILE* out = fopen("test_bytes.dat", "wb");
	char val[] = {'a', 'b', 'c'};
	write_bytes(out, val, 3u);
	fclose(out);
}

void test_write_u16(){
	FILE* out = fopen("test_u16.dat", "wb");
	uint16_t val = 32563;
	write_u16(out, val);
	fclose(out);
}

void test_write_u32(){
	FILE* out = fopen("test_u32.dat", "wb");
	uint32_t val = 65799;
	write_u32(out, val);
	fclose(out);
}

void test_write_s16(){
	FILE* out = fopen("test_s16.dat", "wb");
	int16_t val[] = {-1000, 1650};
	write_s16(out, val[0]);
	write_s16(out, val[1]);
	fclose(out);
}

void test_write_s16_buf(){
	FILE* out = fopen("test_s16_buf.dat", "wb");
	int16_t val[] = {-1000, 1650, 4927};
	write_s16_buf(out, val, 3u);
	fclose(out);
}

// the following is written by bstride1
void test_read_byte(){
  FILE *in = fopen("test_byte.dat", "rb");
  
  char expected[] = {'a', 'b'};
  char actual[2];
  read_byte(in, &(actual[0]));
  read_byte(in, &(actual[1]));

  for (int i = 0; i < 2; i++){
    printf("Expected = %c, actual = %c\n", expected[i], actual[i]);
    assert(expected[i] == actual[i]);
  }
  fclose(in); // remember to fclose. same with all other functions
}

void test_read_bytes(){
  FILE *in = fopen("test_bytes.dat", "rb");
  
  char expected[] = {'a', 'b', 'c'};
  char actual[3];
  read_bytes(in, actual, 3u);

  for (int i = 0; i < 3; i++){
    printf("Expected[%d] = %c, actual[%d] = %c\n", i, expected[i], i, actual[i]);
    assert(expected[i] == actual[i]);
  }
  fclose(in);
}

void test_read_u16(){
  FILE *in = fopen("test_u16.dat", "rb");

  uint16_t expected = 32563;
  uint16_t actual;

  read_u16(in, &actual);

  printf("Expected = %d, actual = %d\n", expected, actual);
  assert(expected = actual);
  fclose(in);
}

void test_read_u32(){
  FILE *in = fopen("test_u32.dat", "rb");

  uint32_t expected = 65799;
  uint32_t actual;

  read_u32(in, &actual);

  printf("Expected = %d, actual = %d\n", expected, actual);
  assert(expected = actual);
  fclose(in);
}

void test_read_s16(){
  // test a positive and negative integer
  FILE *in = fopen("test_s16.dat", "rb");
  
  int16_t expected[] = {-1000, 1650};
  int16_t actual[2];

  read_s16(in, &(actual[0]));
  read_s16(in, &(actual[1]));

  for (int i = 0; i < 2; i++){
    printf("Expected[%d] = %c, actual[%d] = %c\n", i, expected[i], i, actual[i]);
    assert(expected[i] == actual[i]);
  }
  fclose(in);
}

void test_read_s16_buf(){
  // test positive and negative integers
  FILE *in = fopen("test_s16_buf.dat", "rb");
  
  int16_t expected[] = {-1000, 1650, 4927};
  int16_t actual[3];

  read_s16_buf(in, actual, 3u);
 
  for (int i = 0; i < 3; i++){
    printf("Expected[%d] = %c, actual[%d] = %c\n", i, expected[i], i, actual[i]);
    assert(expected[i] == actual[i]);
  }
  fclose(in);
}
